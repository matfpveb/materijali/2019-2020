const mongoose = require('mongoose');

const databaseString = 'mongodb://localhost:27017/users';

mongoose.connect(databaseString, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

mongoose.connection.once('open', function () {
  console.log('Uspesno povezivanje!');
});

mongoose.connection.on('error', (error) => {
  console.log('Greska: ', error);
});
