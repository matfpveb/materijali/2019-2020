# 6. sedmica vežbi

## Reaktivna paradigma programiranja

- Poglavlje 4 u [skripti](https://www.nikolaajzenhamer.rs/assets/pdf/pzv.pdf){:target="_blank"}{:target="_blank"}
- [Primeri vezani za eliminaciju petlji](./eliminacija-petlji/){:target="_blank"}
- [Primeri vezani za biblioteku RxJS](./rxjs){:target="_blank"}
- [Prezentacija iz video lekcije](./prezentacija.ppsx){:target="_blank"}
- [Video lekcija - Nikola Ajzenhamer](https://www.youtube.com/watch?v=3QC45tpjwRk){:target="_blank"}

### Zadaci za proveru znanja

- [Zadaci i pitanja](./zadaci_za_vezbu.md){:target="_blank"}

## Korisne veze

### Eliminacija petlji

- [Metodi iz `Array.prototype`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array#Instance_methods){:target="_blank"}
- [Biblioteka Underscore.js](https://underscorejs.org/){:target="_blank"}

### Reaktivna paradigma programiranja

- [ReactiveX familija biblioteka](http://reactivex.io/){:target="_blank"}

### RxJS

- [Zvanični sajt i dokumentacija](https://rxjs-dev.firebaseapp.com/){:target="_blank"}
- [Detaljne informacije za instalaciju](https://rxjs-dev.firebaseapp.com/guide/installation){:target="_blank"}
- [Vizualizacija RxJS koda u vidu marble dijagrama](https://rxviz.com/){:target="_blank"}
- [Interaktivni dijagrami tokova za RxJS funkcije i operatore](https://rxmarbles.com/){:target="_blank"}
- [Skup resursa za učenje](https://reactive.how/){:target="_blank"}