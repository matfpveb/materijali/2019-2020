class Student
{
	constructor(indeks, ime, prezime)
	{
		this.indeks = indeks;
		this.ime = ime;
		this.prezime = prezime;
	}
}

let smerovi = [
	[
        new Student("1/2017", "Pera", "Peric"),
        new Student("3/2017", "Nikola", "Nikolic")
    ],
    [
        new Student("2/2017", "Jovana", "Jovanovic"),
        new Student("4/2017", "Ana", "Nikolic"),
        new Student("5/2017", "Mirjana", "Lucic"),
        new Student("6/2017", "Stefan", "Jovanovic"),
    ]
];

Array.prototype.concatAll = function() {
    let results = [];

    this.forEach(subArray => {
        subArray.forEach(element => {
            results.push(element);
        });
    });

    return results;
};

console.log(smerovi.concatAll());
