class Student
{
	constructor(indeks, ime, prezime)
	{
		this.indeks = indeks;
		this.ime = ime;
		this.prezime = prezime;
	}
}

let studenti = [
	new Student("1/2017", "Pera", "Peric"),
	new Student("2/2017", "Jovana", "Jovanovic"),
    new Student("3/2017", "Nikola", "Nikolic"),
    new Student("1/2018", "Ana", "Nikolic"),
	new Student("2/2018", "Mirjana", "Lucic"),
	new Student("3/2018", "Stefan", "Jovanovic"),
];

function dohvati2018(studenti)
{
    return studenti.filter(student => {
        let godina = student.indeks.substr(student.indeks.indexOf("/") + 1);
        return godina === "2018";
    });
}

console.log(dohvati2018(studenti));
