# Zadaci sa ispita 

Ovde možete pronaći postavke zadataka sa ispita iz 2019/2020. godine. Uz tekstove zadataka su date i kolekcije koje je potrebno uvesti u MongoDB SUBP. 

Važno je napomenuti sledeće razlike između postavki koje stoje na repozitorijumu i onih na ispitu:

- Na ispitu su studentima na raspolaganju i postavke koda koje izgledaju kao na [primeru ispita](../primer%20ispita/README.md), dok u zadacima na linkovima ispod se te postavke ne nalaze.

- **Na ispitu su svi Node.js i Angular paketi instalirani i odgovarajuće BP su popunjene podacima**, dok studenti moraju da svojim računarima da pokrenu komandu `npm install` u klijentskom i serverskom projektu zasebno, kako bi se svi paketi instalirali lokalno.

### Zadaci

- [Jun 1](./jun1)
- [Jun 2](./jun2)
- [Septembar 1](./septembar1)
- [Septembar 2](./septembar2)
- [Septembar 3](./septembar3)
- [Januar 1](./januar1)
