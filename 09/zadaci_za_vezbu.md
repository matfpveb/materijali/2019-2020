## Angular - Pitanja za deo 3

1. Šta su servisi? Koje vrste servisa postoje?
2. Šta je cilj servisa podataka? Navesti primer.
3. Šta je cilj servisa usluga? Navesti primer.
4. Kojim dekoratorom su servisi dekorisani u Angularu? Šta je njegova uloga?
5. Koliko instanci servisa `ProductService` postoje u aplikaciji `store`? Čime je to garantovano?
6. Šta su fabrike servisa?
7. Šta je rutiranje na klijentu?
8. Na koji način možemo uvesti parametre u `path` u okviru rutiranja?
9. Šta je zamena za `href` kada koristimo rutiranje? Koja je prednost toga?
10. Objasniti ulogu servisa `ActivatedRoute`.
11. Zašto je `paramMap` implementirano kao tok vrednosti, a ne samo kao mapa koja čuva te vrednosti url-a?
12. Šta se dešava ako ne uklonimo pretplatu na neki tok, a obrišemo datu komponentu? Kako možemo sačuvati informaciju o pretplati na neki tok?
13. Šta su filteri? Navesti primere nekih filtera ugrađenih u Angular?
14. Objasniti reaktivni i šablonski pristup kod obrade formulara u Angularu?
15. U reaktivnom pristupu obrade formulara postoje dve reprezentacije za jedan isti formular. Navesti ih i objasniti.
16. Šta je potrebno uraditi da se podaci iz forme ne bi slali na klasičan način već korišćenjem mogućnosti koje Angular ima?
17. Napisati validatorsku funkciju u Angular-u za proveru ispravnosti u polju za lozinku. Lozinka mora imati najmanje 8 karaktera, od toga bar jedno malo slovo, bar jedno veliko slovo, bar jednu cifru i bar jedan specijalni karakter.
