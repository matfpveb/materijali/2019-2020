const http = require('http');

const options = {
  hostname: 'http://localhost:5000/',
  path: '/about.html',
  method: 'GET',
  headers: { Accept: 'text/html' },
};
const client = http.request(options, (response) => {
  console.log('Statusni kod odgovora servera: ', response.statusCode);
});

client.on('error', (error) => {
  console.log('Greska: ', error);
});

client.end();
