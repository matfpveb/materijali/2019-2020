const EventEmitter = require('events');

// kreiranje emitera
class SweetEmitter extends EventEmitter{
}
const sweetEmitter = new SweetEmitter();


// priduzivanje funkcije osluskivaca
sweetEmitter.on('chocolateEvent', (data)=>{
    console.log('With joy we serve: ', data);
});

// emitovanje dogadjaja
sweetEmitter.emit('chocolateEvent', 'cake');
sweetEmitter.emit('chocolateEvent', 'ice cream');
sweetEmitter.emit('chocolateEvent', 'pudding');