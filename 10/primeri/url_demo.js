const url = require('url');

const testURL = new URL('http://testwebsite.com:3000/info/users.html?id=1000&status=active');

// string reprezentacija URL-a
console.log(testURL.href);

// mrezno ime 
console.log(testURL.hostname);

// port 
console.log(testURL.port);

// putanja do trazenog fajla
console.log(testURL.pathname);

// parametri pretrage u tekstualnoj formi
console.log(testURL.search);

// parametri pretrage u formi niza 
console.log(testURL.searchParams);
testURL.searchParams.forEach((name, value) => {
    console.log(name, value);
});





