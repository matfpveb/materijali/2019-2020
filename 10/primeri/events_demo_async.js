const EventEmitter = require('events');

// kreiranje emitera
class MathEmitter extends EventEmitter{
}
const mathEmitter = new MathEmitter();


// priduzivanje funkcije osluskivaca
mathEmitter.on('sum', (a, b)=> {
    setImmediate(() => {
        console.log(`${a}+${b}=${a+b}`);
    });
});

// emitovanje dogadjaja
mathEmitter.emit('sum', 15, 5);
mathEmitter.emit('sum', 15, 7);


