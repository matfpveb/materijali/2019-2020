const http = require('http');
const fs = require('fs');
const path = require('path');

const server = http.createServer((req, res) => {
  let contentFile;
  let contentType;

  contentFile = req.url;
  if (req.url == '/') {
    contentFile = 'index.html';
  }

  const fileExtension = path.extname(contentFile);
  switch (fileExtension) {
    case '.js':
      contentType = 'text/javascript';
      break;
    case '.html':
      contentType = 'text/html';
      break;
    case '.json':
      contentType = 'application/json';
      break;
    case '.css':
      contentType = 'text/css';
      break;
    case '.jpg':
      contentType = 'image/jpg';
      break;
    default:
      contentType = 'text/html';
  }

  const contentPath = path.join(__dirname, 'public', contentFile);

  fs.readFile(contentPath, (err, content) => {
    if (err) {
      if (err.code == 'ENOENT') {
        const path404 = path.join(__dirname, 'public', '404.html');
        fs.readFile(path404, (err, content404) => {
          res.writeHead(200, {
            'Content-Type': 'text/html',
          });
          res.write(content404);
          res.end();
        });
      } else {
        res.writeHead(500, 'Server error!');
        res.end();
      }
    } else {
      res.writeHead(200, {
        'Content-Type': contentType,
      });
      res.write(content);
      res.end();
    }
  });
});

server.listen(5000, () => {
  console.log('Server je pokrenut!');
});
