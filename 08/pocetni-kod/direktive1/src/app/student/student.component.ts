import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  private static readonly listaImena: string[] = [
    'Una',
    'Milica',
    'Marko',
    'Bojan'
  ];
  private static readonly brojImena: number =
    StudentComponent.listaImena.length;

  private ocene: number[] = [];
  public ime: string;
  public ocenaStr: string;

  constructor() {
    this.ime =
      StudentComponent.listaImena[
        Math.floor(Math.random() * StudentComponent.brojImena)
      ];
  }

  ngOnInit() {}

  public prosek(): number {
    return this.ocene.length === 0
      ? 0
      : this.ocene.reduce((prev, next) => prev + next, 0) / this.ocene.length;
  }

  public onDodajOcenu(): void {
    const ocenaNum = Number.parseInt(this.ocenaStr, 10);
    this.ocenaStr = undefined;
    if (Number.isNaN(ocenaNum) || ocenaNum < 5 || ocenaNum > 10) {
      return;
    }
    console.log(this.ocene);
    this.ocene.push(ocenaNum);
  }
}
