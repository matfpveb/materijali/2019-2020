import { Component, OnInit, Input, OnChanges, SimpleChanges, DoCheck, OnDestroy } from '@angular/core';
import { Student, PogresanSmerError } from '../../models/student.model';

@Component({
  selector: 'app-student3',
  templateUrl: './student3.component.html',
  styleUrls: ['./student3.component.css']
})
export class Student3Component implements OnInit, OnChanges, DoCheck, OnDestroy {
  
  @Input('studentData')
  public student: Student;

  constructor() { 
    console.log(`Upisali smo novog studenta na MATF ${this.student === undefined ? 'čije ime nam je nepoznato još uvek' : this.student.getImePrezime()}! (constructor)`);
  }

  ngOnInit() {
    console.log(`Inicijalizacija studenta ${this.student === undefined ? 'čije ime nam je nepoznato još uvek' : this.student.getImePrezime()}! (ngOnInit)`);
  }

  ngOnChanges(simpleChanges: SimpleChanges): void {
    console.log('Promene:\n', simpleChanges, '\n', '(ngOnChanges)');
  }

  ngDoCheck(): void {
    console.log('Doslo je do ciklusa detekcije promene! (ngDoCheck)')
  }

  ngOnDestroy(): void {
    console.log(`Student ${this.student.getImePrezime()} je diplomirao :) (ngOnDestroy)`);
  }

  dohvatiUrlSlike(): string {
    switch (this.student.inicijalSmera) {
      case 'I':
        return 'assets/blue.png';
      case 'M':
        return 'assets/red.png';
      case 'A':
        return 'assets/green.png';
      case 'T':
        return 'assets/gray.png';
      default:
        throw new PogresanSmerError(`Nepoznat inicijal smera ${this.student.inicijalSmera}`);
    }
  }

  onKlikni(): void {
    console.log('Kliknuto je dugme u Student3Component!');
  }

}
