import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { Student } from 'src/models/student.model';

@Component({
  selector: 'app-kreator4',
  templateUrl: './kreator4.component.html',
  styleUrls: ['./kreator4.component.css']
})
export class Kreator4Component implements OnInit {

  @Output('noviStudent')
  public emitNoviStudent: EventEmitter<Student> = new EventEmitter<Student>();

  @ViewChild('imePrezimeInput', {static: false})
  private imePrezimeInput: ElementRef;

  @ViewChild('smerSelect', {static: false})
  private odabranSmerSelect: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  onUpisStudenta(): void {
    const imePrezime: string = (<HTMLInputElement>this.imePrezimeInput.nativeElement).value;
    const odabranSmer: string = (<HTMLSelectElement>this.odabranSmerSelect.nativeElement).value;

    if (imePrezime === '') {
      window.alert('Molimo unesite ime i prezime novog studenta!');
      return;
    }

    const noviStudent = new Student(imePrezime, odabranSmer);
    this.emitNoviStudent.emit(noviStudent);

    // Ne preporucuje se menjanje DOM stabla na ovaj nacin
    (<HTMLInputElement>this.imePrezimeInput.nativeElement).value = '';
  }

}
