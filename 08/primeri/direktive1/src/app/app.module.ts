import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { StudentComponent } from './student/student.component';
import { NgifTestComponent } from './ngif-test/ngif-test.component';
import { NgforTestComponent } from './ngfor-test/ngfor-test.component';
import { NgifelseTestComponent } from './ngifelse-test/ngifelse-test.component';
import { NgswitchTestComponent } from './ngswitch-test/ngswitch-test.component';
import { NgclassTestComponent } from './ngclass-test/ngclass-test.component';
import { NgstyleTestComponent } from './ngstyle-test/ngstyle-test.component';

@NgModule({
  declarations: [
    AppComponent,
    StudentComponent,
    NgifTestComponent,
    NgforTestComponent,
    NgifelseTestComponent,
    NgswitchTestComponent,
    NgclassTestComponent,
    NgstyleTestComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
