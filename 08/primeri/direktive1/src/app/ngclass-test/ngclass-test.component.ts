import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-ngclass-test',
    templateUrl: './ngclass-test.component.html',
    styleUrls: ['./ngclass-test.component.css']
})
export class NgclassTestComponent implements OnInit {

    private static readonly listaImena: Array<string> = ['Una', 'Milica', 'Marko', 'Bojan'];
    private static readonly brojImena: number = NgclassTestComponent.listaImena.length;

    private ocene: Array<number> = [];
    private ime: string = '';
    private ocenaStr: string = null;

    constructor() {
        this.ime = NgclassTestComponent.listaImena[Math.floor(Math.random() * NgclassTestComponent.brojImena)];
    }

    ngOnInit() {
    }

    private prosek(): number {
        if (this.ocene.length === 0) {
            return 0.0;
        }
        return this.ocene.reduce((prev, next) => prev + next) / this.ocene.length;
    }

    private onDodajOcenu(): void {
        if (Number.isNaN(Number.parseInt(this.ocenaStr))) {
            this.ocenaStr = null;
            return;
        }

        const ocenaNum: number = Number.parseInt(this.ocenaStr);
        if (ocenaNum < 5 || ocenaNum > 10) {
            this.ocenaStr = null;
            return;
        }

        this.ocene.push(ocenaNum);
        this.ocenaStr = null;
    }

    private izracunajKlasu() {
        const prosek = this.prosek();
        return {
            'alert': true,
            'alert-success': prosek >= 9,
            'alert-primary': prosek >= 8 && prosek < 9,
            'alert-info': prosek >= 7 && prosek < 8,
            'alert-warning': prosek >= 6 && prosek < 7,
            'alert-danger': prosek < 6 && prosek !== 0,
            'alert-light': prosek === 0
        }
    }

}
