import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-ngfor-test',
    templateUrl: './ngfor-test.component.html',
    styleUrls: ['./ngfor-test.component.css']
})
export class NgforTestComponent implements OnInit {

    private static readonly listaImena: Array<string> = ['Una', 'Milica', 'Marko', 'Bojan'];
    private static readonly brojImena: number = NgforTestComponent.listaImena.length;

    private ocene: Array<number> = [];
    private ime: string = '';
    private ocenaStr: string = null;

    constructor() {
        this.ime = NgforTestComponent.listaImena[Math.floor(Math.random() * NgforTestComponent.brojImena)];
    }

    ngOnInit() {
    }

    private prosek(): number {
        if (this.ocene.length === 0) {
            return 0.0;
        }
        return this.ocene.reduce((prev, next) => prev + next) / this.ocene.length;
    }

    private onDodajOcenu(): void {
        if (Number.isNaN(Number.parseInt(this.ocenaStr))) {
            this.ocenaStr = null;
            return;
        }

        const ocenaNum: number = Number.parseInt(this.ocenaStr);
        if (ocenaNum < 5 || ocenaNum > 10) {
            this.ocenaStr = null;
            return;
        }

        this.ocene.push(ocenaNum);
        this.ocenaStr = null;
    }

    private prikaziProsek: boolean = false;

    private onPromeniPrikazivanjeProseka(): void {
        this.prikaziProsek = !this.prikaziProsek;
    }

}
