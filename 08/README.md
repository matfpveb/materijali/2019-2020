# 8. sedmica vežbi

## Angular

- Sekcije 5.5 i 5.6 u [skripti](https://www.nikolaajzenhamer.rs/assets/pdf/pzv.pdf){:target="_blank"}{:target="_blank"}
- [Početni kod za primere iz skripte](./pocetni-kod/){:target="_blank"}
- [Kompletni primeri iz skripte](./primeri/){:target="_blank"}
- [Prezentacija iz video lekcije](./prezentacija.ppsx){:target="_blank"}
- [Video lekcija - Nikola Ajzenhamer](https://www.youtube.com/watch?v=p1H4Bptc4UM){:target="_blank"}

### Zadaci za proveru znanja

- [Zadaci i pitanja](./zadaci_za_vezbu.md){:target="_blank"}

## Korisne veze

- [Zvanična stranica Angular razvojnog alata](https://angular.io/){:target="_blank"}
- [Biblioteka Bootstrap](https://getbootstrap.com/){:target="_blank"}
