import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { StudentComponent } from './student/student.component';
import { Student2Component } from './student2/student2.component';
import { Student1ListaComponent } from './student1-lista/student1-lista.component';
import { Student3Component } from './student3/student3.component';
import { Student3ListaComponent } from './student3-lista/student3-lista.component';
import { Student4Component } from './student4/student4.component';
import { Student4ListaComponent } from './student4-lista/student4-lista.component';
import { Student5Component } from './student5/student5.component';
import { Student5ListaComponent } from './student5-lista/student5-lista.component';
import { Student6Component } from './student6/student6.component';
import { Student6ListaComponent } from './student6-lista/student6-lista.component';
import { Student7Component } from './student7/student7.component';
import { Student7ListaComponent } from './student7-lista/student7-lista.component';

@NgModule({
  declarations: [
    AppComponent,
    StudentComponent,
    Student2Component,
    Student1ListaComponent,
    Student3Component,
    Student3ListaComponent,
    Student4Component,
    Student4ListaComponent,
    Student5Component,
    Student5ListaComponent,
    Student6Component,
    Student6ListaComponent,
    Student7Component,
    Student7ListaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
