import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-student5',
    templateUrl: './student5.component.html',
    styleUrls: ['./student5.component.css']
})
export class Student5Component implements OnInit {

    static listaImena: Array<string> = ['Ana', 'Nevena', 'Djordje', 'Milos'];
    static listaPrezimena: Array<string> = ['Jovanovic', 'Nikolic', 'Stefanovic', 'Milovanovic'];

    inicijalizovanStudent: boolean = false;
    dugmeJeUkljuceno: boolean = false;
    ime: string;
    prezime: string;

    constructor() {
        setTimeout(() => {
            this.dugmeJeUkljuceno = true;
        }, 2000);
    }

    ngOnInit() {
    }

    onDohvatiInformacije(): void {
        if (this.inicijalizovanStudent) {
            return;
        }
        this.ime = Student5Component.listaImena[Math.floor(Math.random() * Student5Component.listaImena.length)];
        this.prezime = Student5Component.listaPrezimena[Math.floor(Math.random() * Student5Component.listaPrezimena.length)];
        this.inicijalizovanStudent = true;
    }

}
