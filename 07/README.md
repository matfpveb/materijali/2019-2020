# 7. sedmica vežbi

## Angular

- Sekcije 5.1, 5.2, 5.3 i 5.4 u [skripti](https://www.nikolaajzenhamer.rs/assets/pdf/pzv.pdf){:target="_blank"}{:target="_blank"}
- [Primeri iz skripte](./primeri/){:target="_blank"}
- [Prezentacija iz video lekcije](./prezentacija.ppsx){:target="_blank"}
- [Video lekcija - Nikola Ajzenhamer](https://www.youtube.com/watch?v=C-G7wzMEbE4){:target="_blank"}

### Zadaci za proveru znanja

- [Zadaci i pitanja](./zadaci_za_vezbu.md){:target="_blank"}

## Korisne veze

- [Zvanična stranica Angular razvojnog alata](https://angular.io/){:target="_blank"}
- [Biblioteka Bootstrap](https://getbootstrap.com/){:target="_blank"}
- [Spisak interfejsa u Web API-u](https://developer.mozilla.org/en-US/docs/Web/API#Interfaces){:target="_blank"}

## Razne druge reference

- [Zvanična stranica AngularJS (Angular 1) razvojnog alata](https://angularjs.org/){:target="_blank"}
- [Stilizovanje dugmića pomoću Bootstrap biblioteke](https://getbootstrap.com/docs/4.4/components/buttons/){:target="_blank"}
- [Atributi `HtmlInputElement` interfejsa](https://developer.mozilla.org/en-US/docs/Web/API/HTMLInputElement#Properties){:target="_blank"}
- [Događaji `HtmlInputElement` interfejsa](https://developer.mozilla.org/en-US/docs/Web/API/HTMLInputElement#Events){:target="_blank"}
- [Interfejs `Event`](https://developer.mozilla.org/en-US/docs/Web/API/Event){:target="_blank"}
