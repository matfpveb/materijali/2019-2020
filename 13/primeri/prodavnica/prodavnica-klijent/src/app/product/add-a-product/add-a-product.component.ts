import { Product } from '../product.model';
import { ProductService } from '../product.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-a-product',
  templateUrl: './add-a-product.component.html',
  styleUrls: ['./add-a-product.component.css'],
})
export class AddAProductComponent implements OnInit {
  public addAProductForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService
  ) {
    this.addAProductForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      price: ['', [Validators.required, Validators.min(1)]],
      description: [''],
    });
  }

  public submitForm(): void {
    if (!this.addAProductForm.valid) {
      window.alert('Not valid!');
      return;
    }

    this.productService
      .addAProduct(this.addAProductForm.value)
      .subscribe((product: Product) => {
        window.alert('Successfully added a product!');
        console.log(product);
        this.addAProductForm.reset();
      });
  }

  public getNameErrors() {
    return this.addAProductForm.get('name').errors;
  }
  public getPriceErrors() {
    return this.addAProductForm.get('price').errors;
  }

  ngOnInit() {}
}
