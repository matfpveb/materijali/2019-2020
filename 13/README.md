# 13. sedmica vežbi

## Angular

- Poglavlje 9 u [skripti](https://www.nikolaajzenhamer.rs/assets/pdf/pzv.pdf){:target="_blank"}{:target="_blank"}
- [Kompletni primeri iz skripte](./primeri/){:target="_blank"}
- [Video lekcija - Nikola Ajzenhamer](https://www.youtube.com/watch?v=IQs2HKtVWa0){:target="_blank"}

### Zadaci za proveru znanja

- [Zadaci i pitanja](./zadaci_za_vezbu.md){:target="_blank"}

## Korisne veze

- [Zvanična stranica Angular razvojnog alata](https://angular.io/){:target="_blank"}
- [Biblioteka Bootstrap](https://getbootstrap.com/){:target="_blank"}
