// Kreiranje objekata koriscenjem literal notacije.
let a = {
  x: 10,
  y: 5,
  print: function() {
    console.log('Point: ', this.x, this.y);
  }
};

console.log(a.x);
a.print();
