// Navedene primere testirati u pregledacu.

// Primer 1.
// this je window objekat
console.log(this);

// Primer 2.
var x = 20;
function zbir(a, b) {
  return a + b;
}
// promenljive deklarisane sa var i deklaracije funkcija se cuvaju na nivou window objekta
console.log(window.x);
console.log(window.zbir);


// Primer 3. 
// this je window objekat
function test(){
    console.log(this);
}
test();


// Primer 4.
// this je window objekat
(function(){ 
    console.log(this);
})();


// Primer 5.
var x = 10;
var y = 5;
var obj = {
    x: 20, 
    y: 10, 
    zapis: (function(){
        // this je window objekat
        return `x: ${this.x}, y: ${this.y}`;
    })()
}
console.log(obj.x, obj.y, obj.zapis);

// Primer 6.
// this je obj objekat
var x = 100;
var y = 100;
var obj = {
  x: 50,
  y: 30,
  print: function() {
    console.log(this.x, this.y);
  }
};
// rezultat je 50, 30
obj.print(); 


// Primer 7.
// => funkcija ima leksicko this
var x = 100;
var y = 100;
var obj = {
  x: 50,
  y: 30,
  print: () => console.log(this.x, this.y)
};
// rezultat je 100 100
obj.print(); 


// Primer 8.
let sayHello = {
    message: 'Hello', 
    say: function(names){
        names.forEach(function(name){
            // oprez: this je window!
            console.log(this.message, name);
        });
    }
};
sayHello.say(['Ana', 'Lazar', 'Marko', 'Maja']);


// Primer 9.
let sayHello = {
    message: 'Hello', 
    say: function(names){
        // popravka navodjenjem dodatnog argumenta
        names.forEach(function(name){
            console.log(this.message, name);
        }, this);
    }
};
sayHello.say(['Ana', 'Lazar', 'Marko', 'Maja']);

// Primer 10.
let sayHello = {
    message: 'Hello', 
    say: function(names){
        // popravka cuvanjem vrednosti this objekta
        let self = this;
        names.forEach(function(name){
            console.log(self.message, name);
        });
    }
};
sayHello.say(['Ana', 'Lazar', 'Marko', 'Maja']);
