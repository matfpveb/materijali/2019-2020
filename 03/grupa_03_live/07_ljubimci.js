// Osnovna klasa: 
function Ljubimac(ime, godine){
    this.ime = ime;
    this.godine = godine;
}

Ljubimac.prototype.opis = function(){
    return this.ime + ' ' + this.godine;
}

let zile = new Ljubimac('Zile', 2);
console.log(zile.opis());

// Zec koji je ljubimac: 
function Zec(ime, godine, boja){
    Ljubimac.call(this, ime, godine);
    this.boja = boja;
}

Zec.prototype = Object.create(Ljubimac.prototype);
Zec.prototype.constructor = Zec;

Zec.prototype.opis = function(){
    // return this.ime + ' ' + this.godine + ' ' + this.boja;
    return Object.getPrototypeOf(Zec.prototype).opis.call(this)+ ' ' + this.boja;
}

let bane = new Zec('Bane', 3, 'bela');
console.log(bane.opis());

// Pas koji je ljubimac:
function Pas(ime, godine, omiljenaHrana){
    Ljubimac.call(this, ime, godine);
    this.omiljenaHrana = omiljenaHrana;
}
Pas.prototype = Object.create(Ljubimac.prototype);
Pas.prototype.constructor = Pas;

Pas.prototype.opis = function(){
    // return this.ime + ' ' + this.godine + ' ' + this.omiljenaHrana;
    return Object.getPrototypeOf(Pas.prototype).opis.call(this) + ' ' + this.omiljenaHrana;

}

let dzeki = new Pas('Dzeki', 2, 'koska');
console.log(dzeki.opis());

// Eksperimentisati sa: 
// bane.__proto__.opis.call(bane)
// bane.__proto__.__proto__.opis.call(bane)
// bane.__proto__.__proto__.opis.call(dzeki)
// console.log(Object.getPrototypeOf(bane) == bane.__proto__)
// console.log(Object.getPrototypeOf(bane) == dzeki.__proto__)
// Object.getPrototypeOf(Object.getPrototypeOf(bane)) == Object.getPrototypeOf(Object.getPrototypeOf(dzeki))

// obratiti paznju da nam kretanja kroz lanac prototipova tipa bane.__proto__ i bane.__proto__.__proto__ omogucavaju
// da stignemo do zeljene metode, a da nam sam poziv metode omogucava call funkcija

