function Point(x, y) {
  this.x = x;
  this.y = y;
}

Point.prototype.print = function() {
  console.log('Point: ', this.x, this.y);
};

A = new Point(4, 5);
A.print();
