# 11. sedmica vežbi

## Express.js

- [Express.js](./expressjs.md) 
- [primeri sa časa](./primeri/){:target="_blank"}
- [Video lekcija - Anđelka Zečević](https://youtu.be/XOZ9luCtKwo){:target="_blank"}

<!-- ### Zadaci za proveru znanja

- [Zadaci i pitanja](./zadaci_za_vezbu.md){:target="_blank"} -->

## Korisne veze

- [zvanična Express.js dokumentacija](https://expressjs.com/){:target="_blank"}