const users = [
  {
    id: 1,
    username: 'john',
    email: 'john@matf.bg.ac.rs',
    password: 'john123',
    status: 'active',
  },

  {
    id: 2,
    username: 'pavle',
    email: 'pavle@gmail.com',
    password: 'pavle123',
    status: 'active',
  },

  {
    id: 3,
    username: 'maja',
    email: 'maja@gmail.com',
    password: 'maja123',
    status: 'inactive',
  },

  {
    id: 4,
    username: 'klara',
    email: 'klara@gmail.com',
    password: 'klara123',
    status: 'active',
  },
];

module.exports = users;
