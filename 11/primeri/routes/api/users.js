const users = require('../../users.js');
const express = require('express');
const router = express.Router();

// http://localhost:3000/api/users
router.get('/', (req, res) => {
  res.json(users);
});

// http://localhost:3000/api/users/4
router.get('/:id', (req, res) => {
  const isFound = users.some((user) => user.id === parseInt(req.params.id));

  if (isFound) {
    const user = users.filter((user) => user.id == parseInt(req.params.id));
    res.status(200);
    res.json(user);
  } else {
    res.status(404);
    res.send();
  }
});

// http://localhost:3000/api/users
router.post('/', (req, res) => {
  if (!req.body.username || !req.body.password) {
    res.status(400);
    res.send();
  } else {
    const newUser = {
      id: 23,
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      status: 'active',
    };

    users.push(newUser);

    res.status(201).json(newUser);
  }
});

// http://localhost/api:3000/users/4
router.put('/:id', (req, res) => {
  const isFound = users.some((user) => user.id === parseInt(req.params.id));

  if (isFound) {
    const updatePasswordInfo = req.body;

    users.forEach((user) => {
      if (user.id == parseInt(req.params.id)) {
        if (updatePasswordInfo.currentPassword != user.password) {
          res.status(400).send();
        } else {
          user.password = updatePasswordInfo.newPassword;
          res.status(200).send();
        }
      }
    });
  } else {
    res.status(404).send();
  }
});

// http://localhost/api:3000/users/4
router.delete('/:id', (req, res) => {
  const isFound = users.some((user) => user.id === parseInt(req.params.id));

  if (isFound) {
    let deleteUserIndex;
    users.forEach((user, index) => {
      if (user.id === parseInt(req.params.id)) {
        deleteUserIndex = index;
        return;
      }
    });
    users.splice(deleteUserIndex, 1);
    res.status(200).send();
  } else {
    res.status(404).send();
  }
});

// error blok
router.use((req, res, next)=>{
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json({'message': `Nepoznat zahtev ${unknownMethod} ka ${unknownPath}!`});
});

module.exports = router;
