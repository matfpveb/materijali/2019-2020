const express = require('express');
const users = require('./users.js');

const app = express();
const port = 3000;

// http://localhost:3000/api/users
app.get('/api/users', (req, res) => {
  res.status(200);
  res.set('Content-Type', 'application/json');
  res.send(JSON.stringify(users));
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
